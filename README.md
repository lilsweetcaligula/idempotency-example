## Service template

### Running tests

Run the container:
```
$ docker-compose up -d && docker-compose exec app bash
```

Run the db migrations:
```
$ NODE_ENV=test ./node_modules/knex/bin/cli.js migrate:latest
```

Run the tests:
```
$ npm test
```


### Running the application

TODO

