const assert = require('assert-plus')
const http = require('http')
const App = require('../src/app.js')
const Server = require('../src/server.js')
const config = require('../src/config/config.js')
const Db = require('../src/lib/db.js')

describe('server', () => {
  let fake_server, fake_app

  const some_server_port = 3333
  const some_server_host = '7.7.7.7'

  const some_mysql_user = 'abcd'
  const some_mysql_password = '1234'
  const some_mysql_host = 'xyz'
  const some_mysql_database = 'b587at5bp'

  beforeEach(() => {
    fake_server = jasmine.createSpyObj('server', ['listen', 'address'])
    fake_app = jasmine.createSpy('app')

    spyOn(http, 'createServer').and.returnValue(fake_server)
    spyOn(App, 'setup').and.returnValue(fake_app)
    spyOn(config, 'getServerPort').and.returnValue(some_server_port)
    spyOn(config, 'getServerHost').and.returnValue(some_server_host)
    spyOn(config, 'getMySqlUser').and.returnValue(some_mysql_user)
    spyOn(config, 'getMySqlPassword').and.returnValue(some_mysql_password)
    spyOn(config, 'getMySqlHost').and.returnValue(some_mysql_host)
    spyOn(config, 'getMySqlDb').and.returnValue(some_mysql_database)
  })

  it('is initialized at the correct address', async () => {
    const preventCallback = (..._args) => {}
    fake_server.listen.and.callFake(preventCallback)

    const server = await Server.setup()

    expect(http.createServer).toHaveBeenCalledWith(fake_app)

    expect(server.listen).toHaveBeenCalledWith(
      some_server_port, some_server_host, jasmine.any(Function)
    )
  })

  it('logs the address to the console', async () => {
    const fake_server_address = jasmine.createSpy()

    fake_server.address.and.returnValue(fake_server_address)

    fake_server.listen.and.callFake((...args) => {
      const [cb,] = args.slice().reverse()

      assert.func(cb, 'cb')

      spyOn(console, 'log')

      cb()

      expect(console.log).toHaveBeenCalledWith('Server listening at address: %j',
        fake_server_address)
    })

    const server = await Server.setup()

    expect(http.createServer).toHaveBeenCalledWith(fake_app)

    expect(server.listen).toHaveBeenCalledWith(
      some_server_port, some_server_host, jasmine.any(Function)
    )
  })

  it('connects to the mysql instance', async () => {
    spyOn(Db, 'connect')

    await Server.setup()

    expect(Db.connect).toHaveBeenCalledWith({
      user: some_mysql_user,
      password: some_mysql_password,
      host: some_mysql_host,
      database: some_mysql_database
    })
  })
})

