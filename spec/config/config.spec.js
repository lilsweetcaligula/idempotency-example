const path = require('path')
const config = require('../../src/config/config.js')

describe('config', () => {
  describe('fromEnv', () => {
    const value = 'SOME_VALUE'

    beforeEach(() => {
      original_env = process.env
      process.env = { ...original_env, foo: value }
    })

    afterEach(() => {
      process.env = original_env
    })

    it('returns the env var if defined', () => {
      const result = config.fromEnv('foo')
      expect(result).toEqual(value)
    })

    it('throws an error if the value is not defined', () => {
      expect(() => config.fromEnv('bar'))
        .toThrow(new Error('process.env["bar"] is not defined'))
    })
  })

  describe('env var helpers', () => {
    const fake_value = 'FAKE_VALUE'

    beforeEach(() => {
      spyOn(config, 'fromEnv')
    })

    describe('getServerPort', () => {
      it('delegates to the "fromEnv" helper', () => {
        const env_var = 'SERVER_PORT'

        config.fromEnv.withArgs(env_var).and.returnValue(fake_value)

        const result = config.getServerPort()

        expect(config.fromEnv).toHaveBeenCalledWith(env_var)
        expect(result).toEqual(fake_value)
      })
    })

    describe('getServerHost', () => {
      it('delegates to the "fromEnv" helper', () => {
        const env_var = 'SERVER_HOST'

        config.fromEnv.withArgs(env_var).and.returnValue(fake_value)

        const result = config.getServerHost()

        expect(config.fromEnv).toHaveBeenCalledWith(env_var)
        expect(result).toEqual(fake_value)
      })
    })

    describe('getServerHostname', () => {
      it('delegates to the "fromEnv" helper', () => {
        const env_var = 'SERVER_HOSTNAME'

        config.fromEnv.withArgs(env_var).and.returnValue(fake_value)

        const result = config.getServerHostname()

        expect(config.fromEnv).toHaveBeenCalledWith(env_var)
        expect(result).toEqual(fake_value)
      })
    })

    describe('getMySqlUser', () => {
      it('delegates to the "fromEnv" helper', () => {
        const env_var = 'MYSQL_USER'

        config.fromEnv.withArgs(env_var).and.returnValue(fake_value)

        const result = config.getMySqlUser()

        expect(config.fromEnv).toHaveBeenCalledWith(env_var)
        expect(result).toEqual(fake_value)
      })
    })

    describe('getMySqlPassword', () => {
      it('delegates to the "fromEnv" helper', () => {
        const env_var = 'MYSQL_PASSWORD'

        config.fromEnv.withArgs(env_var).and.returnValue(fake_value)

        const result = config.getMySqlPassword()

        expect(config.fromEnv).toHaveBeenCalledWith(env_var)
        expect(result).toEqual(fake_value)
      })
    })

    describe('getMySqlHost', () => {
      it('delegates to the "fromEnv" helper', () => {
        const env_var = 'MYSQL_HOST'

        config.fromEnv.withArgs(env_var).and.returnValue(fake_value)

        const result = config.getMySqlHost()

        expect(config.fromEnv).toHaveBeenCalledWith(env_var)
        expect(result).toEqual(fake_value)
      })
    })

    describe('getMySqlDb', () => {
      it('delegates to the "fromEnv" helper', () => {
        const env_var = 'MYSQL_DB'

        config.fromEnv.withArgs(env_var).and.returnValue(fake_value)

        const result = config.getMySqlDb()

        expect(config.fromEnv).toHaveBeenCalledWith(env_var)
        expect(result).toEqual(fake_value)
      })
    })
  })
})

