const assert = require('assert-plus')
const knex = require('knex')
const DatabaseCleaner = require('database-cleaner')

const TestDb = {
  connect: async () => {
    /* TODO: Use env vars.
     */
    const user     = 'root'
    const password = 'i8t7vi9p'
    const database = 'test_db'
    const host     = 'mysql_test'

    const db = knex({
      client:     'mysql2',
      version:    '10.0',
      connection: { user, password, database, host }
    })

    return db
  },

  clean: async (db) => {
    assert.func(db, 'db')

    const conn = await db.client.acquireConnection()

    const deleteDependenciesInOrder = async () => {
      /* NOTE: All tables that require a particular order of
       * deletion due to various constraints, can be deleted
       * explicitly inside this utility.
       */

      await db('orders').delete()
      await db('customers').delete()
    }

    await deleteDependenciesInOrder()

    return new Promise((resolve, reject) => {
      const db_cleaner = new DatabaseCleaner('mysql')

      db_cleaner.clean(conn, err => {
        db.client.releaseConnection(conn).then(() => {
          if (err) {
            reject(err)
            return 
          }

          resolve()
        }).catch(reject)
      })
    })
  },
}

module.exports = TestDb
