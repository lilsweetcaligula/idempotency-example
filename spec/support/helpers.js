const assert = require('assert-plus')

exports.requireNoCache = (ext_require, src_path) => {
  assert.func(ext_require, 'ext_require')
  assert.string(src_path, 'src_path')

  const lib = ext_require(src_path)

  delete ext_require.cache[ext_require.resolve(src_path)]

  return lib
}

exports.textRowToObject = text_row => ({
  ...text_row
})

