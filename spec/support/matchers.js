const assert = require('assert-plus')

/*
 * NOTE: Define your custom matchers here.
 *
 */

const NONE = {}

module.exports = jasmine => ({
  expectRejected: async (p, error_like = NONE, opts = {}) => {
    assert(p && typeof p.then === 'function', 'p must be a promise')
    assert.object(opts, 'opts')

    try {
      await p 
    } catch (err) {
      if (error_like !== NONE) {
        expect(err).toEqual(error_like)

        if ('message_like' in opts) {
          expect(err.message).toMatch(opts.message_like)
        }
      }

      return
    }

    assert.fail('Expected an error to be thrown.')
  }
})

