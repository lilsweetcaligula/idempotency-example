const assert = require('assert-plus')
const Joi = require('@hapi/joi')

/* NOTE: Declare your factories here.
 *
 */

const Factories = {
  createOrder: async (attrs, opts) => {
    assert.object(attrs, 'attrs')
    assert.object(opts, 'opts')
    assert.func(opts.db, 'opts.db')

    const {
      date,
      uuid,
      qty,
      customer_id = await Factories.createCustomer({}, opts)
    } = Factories.validateAttributesAgainstWhitelist(wl => ({
      date: wl.date().optional().default(new Date()),
      uuid: wl.string().optional().default('123e4567-e89b-12d3-a456-426655440000'),
      qty: wl.number().integer().optional().default(1),
      customer_id: wl.number().integer().optional()
    }), attrs)

    const { db } = opts

    const [id] = await db('orders')
      .insert({ date, uuid, qty, customer_id })
    
    return id
  },

  createCustomer: async (attrs, opts) => {
    assert.object(attrs, 'attrs')
    assert.object(opts, 'opts')
    assert.func(opts.db, 'opts.db')

    const final_attrs = Factories.validateAttributesAgainstWhitelist(wl => ({
      first_name: wl.string().optional().default('Marcus'),
      last_name: wl.string().optional().default('Aurelius'),
      city: wl.string().optional().default('Rome'),
      country: wl.string().optional().default('Italy'),
      phone: wl.string().optional().default('+391231212')
    }), attrs)

    const { db } = opts

    const [id] = await db('customers').insert(final_attrs)
    
    return id
  },

  validateAttributesAgainstWhitelist: (wl_builder, attrs) => {
    assert.func(wl_builder, 'wl_builder')
    assert.object(attrs, 'attrs')

    const result = Joi.object(wl_builder(Joi)).validate(attrs)

    if (result.error) throw new Error(result.error.message)

    return result.value
  }
}

module.exports = Factories
