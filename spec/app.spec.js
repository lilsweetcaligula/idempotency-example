const assert = require('assert-plus')
const supertest = require('supertest')
const uuid = require('uuid')
const App = require('../src/app.js')
const Factories = require('./support/factories.js')
const DbHelpers = require('./support/db.js')
const Helpers = require('./support/helpers.js')

describe('App', () => {
  let db

  beforeAll(async () => {
    db = await DbHelpers.connect()
  })

  afterEach(async () => {
    await DbHelpers.clean(db)
  })

  describe('.setup', () => {
    it('initializes the application', () => {
      const fake_db = { _fake: 'db' }
      const app_opts = { db: fake_db }

      const app = App.setup(app_opts)

      expect(app).toEqual(jasmine.any(Function))
    })
  })

  describe('POST /orders', () => {
    let customer_id

    const requestCreateOrder = (app, { customer_id } = {}) => {
      assert.number(customer_id, 'customer_id')

      const SLASH = '/'
      const url = SLASH + ['customers', customer_id, 'orders'].join(SLASH)

      return supertest(app).post(url)
    }

    const buildParams = async (overrides, opts) => {
      assert.object(overrides, 'overrides')

      const {
        date,
        uuid,
        qty,
        request_sent_at
      } = Factories.validateAttributesAgainstWhitelist(wl => ({
        date: wl.any().optional().default(new Date(2005, 0, 1)),
        uuid: wl.any().optional().default('123e4567-e89b-12d3-a456-426655440000'),
        qty: wl.any().optional().default(1),
        request_sent_at: wl.any().optional().default(new Date())
      }), overrides)

      return { date, uuid, qty, request_sent_at }
    }

    const buildApp = () => App.setup({ db })
      .use((err, req, res, next) => {
        console.error(err)
        return next(err)
      })

    beforeEach(async () => {
      customer_id = await Factories.createCustomer({}, { db })
    })

    describe('validation of the request parameters', () => {
      describe('the "date" param', () => {
        it('is required', async () => {
          const app = buildApp()
          const params = await buildParams({}, { db })

          delete params.date

          await requestCreateOrder(app, { customer_id })
            .send(params)
            .expect(422)
            .expect(res => {
              expect(res.body).toEqual({
                error: { message: '"date" is required' }
              })
            })
        })

        it('must be an ISO datestring', async () => {
          const app = buildApp()
          const params = await buildParams({ date: 'foo' }, { db })
          
          await requestCreateOrder(app, { customer_id })
            .send(params)
            .expect(422)
            .expect(res => {
              expect(res.body).toEqual({
                error: { message: '"date" must be in ISO 8601 date format' }
              })
            })
        })
      })

      describe('the "uuid" param', () => {
        it('is required', async () => {
          const app = buildApp()
          const params = await buildParams({}, { db })

          delete params.uuid
          
          await requestCreateOrder(app, { customer_id })
            .send(params)
            .expect(422)
            .expect(res => {
              expect(res.body).toEqual({
                error: { message: '"uuid" is required' }
              })
            })
        })

        it('must be a string', async () => {
          const app = buildApp()
          const params = await buildParams({ uuid: 123 }, { db })
          
          await requestCreateOrder(app, { customer_id })
            .send(params)
            .expect(422)
            .expect(res => {
              expect(res.body).toEqual({
                error: { message: '"uuid" must be a string' }
              })
            })
        })

        it('is required', async () => {
          const app = buildApp()
          const params = await buildParams({}, { db })

          delete params.uuid
          
          await requestCreateOrder(app, { customer_id })
            .send(params)
            .expect(422)
            .expect(res => {
              expect(res.body).toEqual({
                error: { message: '"uuid" is required' }
              })
            })
        })
      })

      describe('the "qty" param', () => {
        it('is required', async () => {
          const app = buildApp()
          const params = await buildParams({}, { db })

          delete params.qty
          
          await requestCreateOrder(app, { customer_id })
            .send(params)
            .expect(422)
            .expect(res => {
              expect(res.body).toEqual({
                error: { message: '"qty" is required' }
              })
            })
        })

        it('must be an integer', async () => {
          const app = buildApp()
          const params = await buildParams({ qty: 3.14159 }, { db })
          
          await requestCreateOrder(app, { customer_id })
            .send(params)
            .expect(422)
            .expect(res => {
              expect(res.body).toEqual({
                error: { message: '"qty" must be an integer' }
              })
            })
        })
      })

      describe('the "request_sent_at" param', () => {
        it('is required', async () => {
          const app = buildApp()
          const params = await buildParams({}, { db })

          delete params.request_sent_at

          await requestCreateOrder(app, { customer_id })
            .send(params)
            .expect(422)
            .expect(res => {
              expect(res.body).toEqual({
                error: { message: '"request_sent_at" is required' }
              })
            })
        })

        it('must be an ISO datestring', async () => {
          const app = buildApp()
          const params = await buildParams({ request_sent_at: 'foo' }, { db })
          
          await requestCreateOrder(app, { customer_id })
            .send(params)
            .expect(422)
            .expect(res => {
              expect(res.body).toEqual({
                error: { message: '"request_sent_at" must be in ISO 8601 date format' }
              })
            })
        })
      })
    })

    describe('on success', () => {
      it('creates a new order', async () => {
        const app = buildApp()
        const params = await buildParams({}, { db })

        expect(await countOrders({ db })).toEqual(0)

        await requestCreateOrder(app, { customer_id })
          .send(params)
          .expect(201)

        expect(await countOrders({ db })).toEqual(1)
      })
    })

    describe('when a new order is successfully created', () => {
      const date = new Date(1900, 0, 1)
      const order_uuid = uuid.v4()
      const qty = 3

      it('has the given values', async () => {
        const app = buildApp()

        const params = await buildParams({
          qty,
          date: date.toISOString(),
          uuid: order_uuid
        }, { db })

        await requestCreateOrder(app, { customer_id })
          .send(params)
          .expect(async (res) => {
            expect(res.body).toEqual(jasmine.objectContaining({
              id: jasmine.any(Number)
            }))

            const { id: order_id } = res.body
            const order = await db('orders').where('id', order_id).first()

            expect(Helpers.textRowToObject(order)).toEqual({
              id: jasmine.any(Number),
              date,
              uuid: order_uuid,
              qty,
              customer_id,
              created_at: jasmine.any(Date),
              updated_at: jasmine.any(Date)
            })
          })
      })
    })

    describe('when the order with the given "uuid" already exists', () => {
      let order_id

      const order_uuid = uuid.v4()

      const date = new Date(1950, 0, 1)
      const qty = 999

      const new_date = new Date(2000, 0, 1)
      const new_qty = 246246

      assert(new_date.getTime() !== date.getTime(),
        'new_date must be different from the original date')

      assert(new_qty !== qty,
        'new_qty must be different from the original quantity')

      beforeEach(async () => {
        order_id = await Factories.createOrder({
          uuid: order_uuid,
          date: date.toISOString(),
          qty
        }, { db })
      })

      it('does not create a new order', async () => {
        const app = buildApp()
        const params = await buildParams({ uuid: order_uuid }, { db })

        expect(await countOrders({ db })).toEqual(1)

        for (const i of [1, 2, 3]) {
          await requestCreateOrder(app, { customer_id })
            .send(params)
            .expect(200)
        }

        const order = await db('orders').where('id', order_id).first()

        expect(order).toEqual(jasmine.any(Object))
        expect(await countOrders({ db })).toEqual(1)
      })

      it('updates the existing order', async () => {
        const app = buildApp()

        const params = await buildParams({
          uuid: order_uuid,
          date: new_date.toISOString(),
          qty: new_qty
        }, { db })

        expect(await countOrders({ db })).toEqual(1)

        await requestCreateOrder(app, { customer_id })
          .send(params)
          .expect(async (res) => {
            expect(res.body).toEqual(jasmine.objectContaining({
              id: jasmine.any(Number)
            }))

            const { id: order_id } = res.body
            const order = await db('orders').where('id', order_id).first()

            expect(order.uuid).toEqual(order_uuid)

            expect(order).toEqual(jasmine.objectContaining({
              date: new_date,
              qty: new_qty
            }))
          })
      })

      describe('when the request to update was sent before the order was updated', () => {
        it('does not update the existing order', async () => {
          const new_date = new Date(2000, 0, 1)
          const new_qty = 246246

          const app = buildApp()

          const params = await buildParams({
            uuid: order_uuid,
            date: new_date.toISOString(),
            qty: new_qty,
            request_sent_at: new Date(1950, 0, 1)
          }, { db })

          expect(await countOrders({ db })).toEqual(1)

          await requestCreateOrder(app, { customer_id })
            .send(params)
            .expect(async (res) => {
              expect(res.body).toEqual(jasmine.objectContaining({
                id: jasmine.any(Number)
              }))

              const { id: order_id } = res.body
              const order = await db('orders').where('id', order_id).first()

              expect(order.uuid).toEqual(order_uuid)

              expect(order).toEqual(jasmine.objectContaining({ date, qty }))
            })
        })
      })
    })

    describe('when an order with a different "uuid" already exists', () => {
      beforeEach(async () => {
        await Factories.createOrder({}, { db })
      })

      it('creates a new order', async () => {
        const order_uuid = uuid.v4()

        const app = buildApp()
        const params = await buildParams({ uuid: order_uuid }, { db })

        expect(await countOrders({ db })).toEqual(1)

        await requestCreateOrder(app, { customer_id })
          .send(params)
          .expect(201)

        expect(await countOrders({ db })).toEqual(2)
      })
    })
  })

  const countOrders = async (opts) => {
    assert.object(opts, 'opts')
    assert.func(opts.db, 'opts.db')

    const { db } = opts
    const [{ count },] = await db('orders').count({ count: '*' })

    return count
  }
})

