const { asyncHandler } = require('../../src/lib/express_utils.js')
const { expectRejected } = require('../support/matchers.js')(jasmine)

describe('Express utils', () => {
  describe('asyncHandler', () => {
    const fake_req = { _fake: 'request' }
    const fake_res = { _fake: 'response' }

    it('has the result return the promise from the handler', async () => {
      const next = jasmine.createSpy()
      const async_handler = asyncHandler(async (...args) => {})
      
      const result = async_handler(fake_req, next, next)

      expect(result).toEqual(jasmine.any(Promise))

      await result
    })

    it('throws an error if the handler is not returning a promise', () => {
      const next = jasmine.createSpy()
      const async_handler = asyncHandler((...args) => {})

      const expected_error = new Error(
        'Expected the handler to be async, i.e. return a promise.'
      )

      expect(() => async_handler(fake_req, fake_res, next))
        .toThrow(expected_error)
    })

    it('does not handle the error if no handler has been provided', async () => {
      const some_error = new Error('oops')

      const async_handler = asyncHandler(async (...args) => {
        throw some_error
      })

      await expectRejected(async_handler(fake_req, fake_res), some_error)
    })

    it('passes the error to the handler if one has been provided', async () => {
      const next = jasmine.createSpy()
      const some_error = new Error('oops')

      const async_handler = asyncHandler(async (...args) => {
        throw some_error
      })

      await async_handler(fake_req, fake_res, next)

      expect(next).toHaveBeenCalledWith(some_error)
    })
  })
})
