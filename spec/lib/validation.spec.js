const { validateWith } = require('../../src/lib/validation.js')
const { expectRejected } = require('../support/matchers.js')(jasmine)

describe('validation utils', () => {
  describe('validateWith', () => {
    describe('result', () => {
      it('does not wrap the error inside Errors.ValidationError if unknown', async () => {
        const fake_schema = jasmine.createSpyObj('schema', ['validateAsync'])
        const some_object = { foo: 'bar' }
        const unknown_error = new Error('unknown error')

        fake_schema.validateAsync.withArgs(jasmine.any(Object))
          .and.returnValue(Promise.reject(unknown_error))

        const validator = validateWith(_schema => fake_schema)

        await expectRejected(validator(some_object), unknown_error)
      })
    })
  })
})

