module.exports = {
  test: {
    client: 'mysql2',
    connection: {
      user:     'root',
      password: 'i8t7vi9p',
      database: 'test_db',
      host:     'mysql_test'
    },
    migrations: {
      directory: './migrations',
      tableName: 'knex_migrations'
    }
  },

  development: {
    client: 'mysql2',
    connection: {
      user:     'root',
      password: 'zw0b58btgr',
      database: 'dev_db',
      host:     'mysql_dev'
    },
    migrations: {
      directory: './migrations',
      tableName: 'knex_migrations'
    }
  }
}
