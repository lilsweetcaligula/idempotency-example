const { timestamps } = require('./lib/timestamps.js')

exports.up = function (knex) {
  return knex.schema.createTable('customers', table => {
    table.increments('id').primary()

    table.string('first_name', 32).collate('utf8mb4_unicode_ci').notNullable()
    table.string('last_name', 32).collate('utf8mb4_unicode_ci').notNullable()
    table.string('city', 16).collate('utf8mb4_unicode_ci').notNullable()
    table.string('country', 32).collate('utf8mb4_unicode_ci').notNullable()
    table.string('phone', 20).collate('utf8mb4_unicode_ci').notNullable()

    timestamps(knex, table)
  })
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('customers')
}

