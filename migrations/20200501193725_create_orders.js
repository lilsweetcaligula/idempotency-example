const { timestamps } = require('./lib/timestamps.js')

exports.up = function (knex) {
  return knex.schema.createTable('orders', table => {
    table.increments('id').primary()

    table.datetime('date').notNullable()
    table.string('uuid', 36).collate('utf8mb4_unicode_ci').notNullable().unique()
    table.integer('qty').notNullable()

    timestamps(knex, table)
  })
}

exports.down = function (knex) {
  return knex.schema.dropTableIfExists('orders')
}

