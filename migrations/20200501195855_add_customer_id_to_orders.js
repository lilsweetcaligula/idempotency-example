const { timestamps } = require('./lib/timestamps.js')

exports.up = function (knex) {
  return knex.schema.alterTable('orders', table => {
    table.integer('customer_id').unsigned().notNullable()
    table.foreign('customer_id').references('customers.id')
  })
}

exports.down = function (knex) {
  return knex.schema.alterTable('orders', table => {
    table.dropForeign('customer_id')
    table.dropColumn('customer_id')
  })
}

