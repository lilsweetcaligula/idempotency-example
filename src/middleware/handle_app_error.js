const {
  ValidationError,
  UnauthorizedError
} = require('../lib/errors.js')

exports.handleAppError = () => (err, req, res, next) => {
  if (res.headersSent) {
    next(err)
    return
  }

  /* NOTE: Please put your handling logic of custom errors
   * below this line.
   */

  if (err instanceof ValidationError) {
    res.status(422).json({ error: { message: err.message } })
    return
  }

  if (err instanceof UnauthorizedError) {
    res.status(401).json({ error: { message: err.message } })
    return
  }

  next(err)

  return
}
