const assert = require('assert-plus')
const { asyncHandler } = require('../../lib/express_utils.js')
const { validateWith } = require('../../lib/validation.js')

exports.validateBody = schema_builder => {
  return asyncHandler(async (req, res, next) =>
    validateWith(schema_builder)(req.body).then(_ => next()).catch(next))
}

