const Express = require('express')
const CreateOrder = require('./create/handler.js')

exports.setup = (app_opts = {}) => {
  const handleCreateOrder = CreateOrder.setup(app_opts) 

  return Express.Router()
    .use(Express.json())
    .post('/customers/:customer_id/orders', handleCreateOrder)
}
