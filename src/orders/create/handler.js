const assert = require('assert-plus')
const Express = require('express')
const { validateBody } = require('../..//middleware/validation/validate_body.js')
const { asyncHandler, transformParams } = require('../../lib/express_utils.js')

exports.setup = app_opts =>
  Express.Router()
    .post(
      '/customers/:customer_id/orders',

      transformParams(Number, ['customer_id']),

      validateBody(schema => schema.object({
        date: schema.date().iso().required(),
        uuid: schema.string().required(),
        qty: schema.number().integer().required(),
        request_sent_at: schema.date().iso().required(),
      })),

      asyncHandler(async (req, res) => {
        assert.object(app_opts, 'app_opts')
        assert.func(app_opts.db, 'app_opts.db')

        const { db } = app_opts
        const { date, qty, uuid: order_uuid } = req.body
        const { customer_id } = req.params

        const [insertion,] = await db.raw(`
          INSERT INTO orders (date, uuid, qty, customer_id)
          SELECT * FROM (
            SELECT :date AS date,
                   :order_uuid AS uuid,
                   :qty AS qty,
                   :customer_id AS customer_id
          ) AS params
          WHERE NOT EXISTS (
            SELECT * FROM orders
            WHERE orders.uuid = params.uuid
          )
        `, { date, qty, customer_id, order_uuid })

        const created_new = insertion.affectedRows > 0

        if (created_new) {
          const id = insertion.insertId
          return res.status(201).json({ id })
        }

        const { request_sent_at } = req.body

        await db('orders')
          .update({ date, qty, customer_id })
          .where('uuid', order_uuid)
          .where('updated_at', '<', request_sent_at)

        const order = await db('orders').where('uuid', order_uuid).first()
        const id = order.id

        return res.status(200).json({ id })
      })
    )

