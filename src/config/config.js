const assert = require('assert-plus')
const fs = require('fs')
const path = require('path')

const config = {
  getServerPort: () => config.fromEnv('SERVER_PORT'),

  getServerHost: () => config.fromEnv('SERVER_HOST'),

  getServerHostname: () => config.fromEnv('SERVER_HOSTNAME'),

  getMySqlUser: () => config.fromEnv('MYSQL_USER'),

  getMySqlPassword: () => config.fromEnv('MYSQL_PASSWORD'),

  getMySqlHost: () => config.fromEnv('MYSQL_HOST'),

  getMySqlDb: () => config.fromEnv('MYSQL_DB'),

  fromEnv: env_var => {
    assert.string(env_var, 'env_var')

    if (env_var in process.env) {
      return process.env[env_var]
    }

    throw new Error('process.env["' + env_var + '"] is not defined')
  }
}

module.exports = config
