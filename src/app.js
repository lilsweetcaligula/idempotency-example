const assert = require('assert-plus')
const Express = require('express')
const OrdersApi = require('./orders/routes.js')
const { handleAppError } = require('./middleware/handle_app_error.js')

exports.setup = (app_opts = {}) => {
  assert.object(app_opts, 'app_opts')
  assert('db' in app_opts, 'app_opts.db')

  const orders = OrdersApi.setup(app_opts)

  return Express()
    .use(Express.json())
    .use(orders)
    .use(handleAppError())
}
