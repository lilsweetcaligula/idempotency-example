const assert = require('assert-plus')
const http = require('http')
const App = require('./app.js')
const Db = require('./lib/db.js')
const config = require('./config/config.js')

exports.setup = async () => {
  const db = await connectToMySql()
  const app = makeApp({ app_context: db })
  const server = makeServer(app)

  return server
}

const makeApp = opts => App.setup(opts)

const makeServer = app => {
  const host = config.getServerHost()
  const port = config.getServerPort()

  const server = http.createServer(app)

  server.listen(port, host, () => {
    console.log('Server listening at address: %j', server.address())
  })
  
  return server
}

const connectToMySql = async () => {
  const user = config.getMySqlUser()
  const password = config.getMySqlPassword()
  const host = config.getMySqlHost()
  const database = config.getMySqlDb()

  return Db.connect({ user, password, host, database })
}

