exports.ValidationError = class extends Error {}

exports.UnauthorizedError = class extends Error {}

exports.AuthTokenExpiredError = class extends Error {}
