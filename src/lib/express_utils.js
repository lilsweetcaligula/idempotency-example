const assert = require('assert-plus')

const ExpressUtils = {
  asyncHandler(handler) {
    assert.func(handler, 'handler')

    return (...args) => {
      const p = handler(...args)
        
      if (p && p.constructor === Promise) {
        const [next,] = args.slice().reverse()

        if (typeof next !== 'function') {
          return p
        }

        return p.catch(next)
      }

      throw new Error('Expected the handler to be async, i.e. return a promise.')
    }
  },

  transformParams: (f, names) => {
    assert.func(f, 'f')
    assert.array(names, 'names')

    return (req, res, next) => {
      for (const name of names) {
        if (name in req.params) {
          req.params[name] = f(req.params[name])
        }
      }
      
      return next()
    }
  }
}

module.exports = ExpressUtils
