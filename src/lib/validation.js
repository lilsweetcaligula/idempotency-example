const assert = require('assert-plus')
const Joi = require('@hapi/joi')
const Errors = require('./errors.js')

const Validation = {
  validateWith: schema_builder => {
    assert.func(schema_builder, 'schema_builder')

    return body => schema_builder(Joi).validateAsync(body)
      .catch(err => {
        if (err && err.constructor === Joi.ValidationError) {
          throw new Errors.ValidationError(err.message)
        }

        throw err
      })
  }
}

module.exports = Validation
