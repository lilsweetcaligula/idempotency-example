const assert = require('assert-plus')
const knex = require('knex')

const Db = {
  connect: async (params, opts = {}) => {
    assert.object(params, 'params')
    assert.string(params.user, 'params.user')
    assert.string(params.password, 'params.password')
    assert.string(params.database, 'params.database')
    assert.string(params.host, 'params.host')

    const { user, password, database, host } = params

    const connector = (() => {
      const DEFAULT_CONNECTOR = knex

      if ('connector' in opts) {
        assert.func(opts.connector, 'opts.connector')
        return opts.connector
      }

      return DEFAULT_CONNECTOR
    })()

    const db = connector({
      client:     'mysql2',
      version:    '10.0',
      connection: { user, password, database, host }
    })

    return db
  }
}

module.exports = Db
